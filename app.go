package main

import (
	"context"
	"goopil-notes/lib/localFs"
	"goopil-notes/lib/md"
	"log"
)

type App struct {
	ctx      context.Context
	notePath string
}

func NewApp() *App {
	return &App{}
}

// startup is called at application startup
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
}

// domReady is called after the front-end dom has been loaded
func (a App) domReady(ctx context.Context) {
	// Add your action here
}

// shutdown is called at application termination
func (a *App) shutdown(ctx context.Context) {
	// Perform your teardown here
}

func (a *App) RenderMarkdown(content string) string {
	return md.Render(content)
}

type Note struct {
	Title string `json:"title"`
	Md    string `json:"md"`
}

func (a *App) NoteList() []Note {
	var notes []Note
	files, _ := localFs.GetFileNames()

	for _, file := range files {
		var n Note
		n.Title = file
		notes = append(notes, n)
	}

	return notes
}

func (a *App) StoreNote(note Note) error {
	err := localFs.SaveFile(note.Title, note.Md)

	if err != nil {
		log.Fatal(err)
	}

	return err
}

func (a *App) ReadNote(title string) (Note, error) {
	content, err := localFs.ReadFile(title)

	if err != nil {
		log.Fatal(err)
	}

	var n Note
	n.Title = title
	n.Md = string(content)

	return n, err
}

func (a *App) RemoveNote(title string) error {
	err := localFs.DeleteFile(title)

	if err != nil {
		log.Fatal(err)
	}

	return err
}

func (a *App) MoveNote(from string, to string) error {
	err := localFs.RenameFile(from, to)

	if err != nil {
		log.Fatal(err)
	}

	return err
}
