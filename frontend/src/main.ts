// FILE: main.js

import {createApp} from 'vue'
import {Quasar} from 'quasar'

import 'quasar/src/css/index.sass'
import quasarIconSet from 'quasar/icon-set/svg-material-icons'

import '@quasar/extras/roboto-font-latin-ext/roboto-font-latin-ext.css'
import '@quasar/extras/material-icons/material-icons.css'
import './assets/scss/main.scss'

import App from './App.vue'
import router from './router/index'

createApp(App)
    .use(router)
    .use(Quasar, {
        plugins: {},
        iconSet: quasarIconSet,
    })
    .mount('#app')
