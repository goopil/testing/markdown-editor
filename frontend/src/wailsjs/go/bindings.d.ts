import * as models from './models';

export interface go {
  "main": {
    "App": {
		MoveNote(arg1:string,arg2:string):Promise<Error>
		NoteList():Promise<Array<models.Note>>
		ReadNote(arg1:string):Promise<models.Note|Error>
		RemoveNote(arg1:string):Promise<Error>
		RenderMarkdown(arg1:string):Promise<string>
		StoreNote(arg1:models.Note):Promise<Error>
    },
  }

}

declare global {
	interface Window {
		go: go;
	}
}
