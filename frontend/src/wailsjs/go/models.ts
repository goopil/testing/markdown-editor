/* Do not change, this code is generated from Golang structs */

export {};

export class Note {
    title: string;
    md: string;

    static createFrom(source: any = {}) {
        return new Note(source);
    }

    constructor(source: any = {}) {
        if ('string' === typeof source) source = JSON.parse(source);
        this.title = source["title"];
        this.md = source["md"];
    }
}

