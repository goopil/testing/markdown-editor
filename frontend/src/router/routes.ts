import DefaultLayout from '../layouts/default.vue';

import NoteView from '../views/Note.vue';

export const routes = [
    {
        path: '/',
        component: DefaultLayout,
        children: [
            {
                path: '',
                name: 'Home',
                component: NoteView,
            },
            {
                path: ':path',
                name: 'Note',
                component: NoteView,
            }
        ]
    }
];
