package localFs

import (
	"log"
	"os"
	"path/filepath"
	"strings"
)

var basePath = "goopil-notes"
var noteFolder = "notes"

func getUserFolder() string {
	dirname, err := os.UserHomeDir()

	if err != nil {
		log.Fatal(err)
	}

	return dirname
}

func getNoteBasePath() string {
	path := getUserFolder()
	outPath := filepath.Join(path, basePath, noteFolder)

	if _, err := os.Stat(outPath); os.IsNotExist(err) {
		err := os.MkdirAll(outPath, 0755)

		if err != nil {
			log.Fatal(err)
		}
	}

	return outPath
}

func formatPath(filename string) string {
	return filepath.Join(getNoteBasePath(), filename+".md")
}

func GetFileNames() ([]string, error) {
	var filesNames []string

	err := filepath.Walk(getNoteBasePath(), func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			filesNames = append(filesNames, strings.TrimSuffix(filepath.Base(path), filepath.Ext(path)))
		}

		return nil
	})

	return filesNames, err
}

func SaveFile(name string, content string) error {
	err := os.WriteFile(formatPath(name), []byte(content), 0755)

	return err
}

func ReadFile(name string) (string, error) {
	content, err := os.ReadFile(formatPath(name))

	return string(content), err
}

func DeleteFile(name string) error {
	err := os.RemoveAll(formatPath(name))

	return err
}

func RenameFile(from string, to string) error {
	err := os.Rename(formatPath(from), formatPath(to))

	return err
}
