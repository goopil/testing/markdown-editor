package md

import (
	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/parser"
)

func Render(content string) string {
	p := parser.NewWithExtensions(parser.CommonExtensions | parser.AutoHeadingIDs)
	htmlAsByte := markdown.ToHTML([]byte(content), p, nil)

	return string(htmlAsByte)
}
