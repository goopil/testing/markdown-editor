module goopil-notes

go 1.17

require (
	github.com/gomarkdown/markdown v0.0.0-20220114203417-14399d5448c4
	github.com/wailsapp/wails/v2 v2.0.0-beta.34
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fasthttp/websocket v1.4.5 // indirect
	github.com/gabriel-vasile/mimetype v1.4.0 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/gofiber/fiber/v2 v2.25.0 // indirect
	github.com/gofiber/websocket/v2 v2.0.15 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/jchv/go-winloader v0.0.0-20210711035445-715c2860da7e // indirect
	github.com/klauspost/compress v1.14.1 // indirect
	github.com/leaanthony/go-ansi-parser v1.2.0 // indirect
	github.com/leaanthony/go-common-file-dialog v1.0.3 // indirect
	github.com/leaanthony/go-webview2 v1.0.3-0.20220314105146-f44268990abe // indirect
	github.com/leaanthony/gosod v1.0.3 // indirect
	github.com/leaanthony/slicer v1.6.0 // indirect
	github.com/leaanthony/typescriptify-golang-structs v0.1.7 // indirect
	github.com/leaanthony/winc v0.0.0-20220208061147-37b059b9dc3b // indirect
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/savsgio/gotils v0.0.0-20211223103454-d0aaa54c5899 // indirect
	github.com/tkrajina/go-reflector v0.5.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.32.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/net v0.0.0-20220121210141-e204ce36a2ba // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
)

// replace github.com/wailsapp/wails/v2 v2.0.0-beta.21 => /Users/zvolpi/go/pkg/mod/github.com/wailsapp/wails/v2@v2.0.0-beta.21
